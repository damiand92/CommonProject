from os.path import join, isfile, basename
from os import listdir
from pathlib import Path

project_directory = 'SeleniumPython'
tests_directory = 'Tests'
tests_dir = join(project_directory, tests_directory)
tests_path = join(Path(__file__).parents[0], tests_dir)
full_path_files = list()

only_files = [f for f in listdir(tests_path) if isfile(join(tests_path, f))]
for i in only_files:
    full_path_files.append(join(tests_path, i))
full_path_files.sort()
for i in full_path_files:
    with open(i, 'r') as file:
        actual_opened_filename = basename(i)
        print("File {} open, path is {}".format(actual_opened_filename, i))
        exec(file.read())