import time
from os.path import basename
from inspect import currentframe
from sys import path, argv
from pathlib import Path

path.append(str(Path(__file__).parents[2])) # added path of main directory to sys path
from SeleniumPython.Framework.init_class import InitClass
from SeleniumPython.Framework.login_page import LoginPage
from SeleniumPython.Framework.enum import ExecutionResults


class FirstTest001(LoginPage):
    """
    FirstTest001 - class with test case methods
    """
    def __init__(self, log_path: str):
        """
        init function to get all the JSON variables
        :param log_path: path of the log file
        """
        super().__init__(log_path)
        self.url = json_load.get('url_main')

    def __enter__(self):
        """
        enter function to set start time and print log file header
        """
        self.start_time = time.time()
        self.log(log_header)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exit function to measure and print test case time
        """
        self.end_time = time.time()
        self.log(("Test Case {} elapsed time: {:.3f} s\n".format(test_name[0], self.end_time-self.start_time)))

    def pre_condition_01(self):
        """
        pre_condition_01 function to open and verify that expected page is opened

        :return: returns PASS if expected page matches current page, otherwise returns ERROR
        """
        self.open_page(self.url)
        if self.verify_full_url(self.url):
            self.log("Result of {} is PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("Result of {} is ERROR\n".format(currentframe().f_code.co_name))
            return ExecutionResults.ERROR
        
    def test_case_01(self):
        """
        test_case_01 function to verify that expected page is opened

        :return: returns PASS if expected page matches current page, otherwise returns FAIL
        """
        if self.verify_full_url(self.url):
            self.log("Result of {} is PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("Result of {} is FAIL\n".format(currentframe().f_code.co_name))
            return ExecutionResults.FAIL

    def post_condition_01(self):
        """
        post_condition_01 function to verify that expected page is opened

        :return: returns PASS if expected page matches current page, otherwise returns ERROR
        """
        if self.verify_full_url(self.url):
            self.log("Result of {} is PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("Result of {} is ERROR\n".format(currentframe().f_code.co_name))
            return ExecutionResults.ERROR


# if executed is test_run.py file then test name is assigned from 'actual_opened_filename' variable from test_run.py
# file, otherwise test name is name of the executed file in configuration
if (basename(__file__)) == 'test_run.py':
    test_name = currentframe().f_globals.get('actual_opened_filename').split(".")
else:
    test_name = basename(__file__).split(".")

init = InitClass()

# name of imported JSON
json_name = argv[1]
# get log path, header and json file from initialization function of InitClass
path_log, log_header, json_load = init.test_initialization(test_name, json_name)

# open test case as context manager
with FirstTest001(path_log) as first_test_001:
    init.execute_all_methods(first_test_001)