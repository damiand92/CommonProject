import time
from os.path import basename
from inspect import currentframe
from sys import path, argv
from pathlib import Path

path.append(str(Path(__file__).parents[2])) # added path of main directory to sys path
from SeleniumPython.Framework.init_class import InitClass
from SeleniumPython.Framework.login_page import LoginPage
from SeleniumPython.Framework.enum import ExecutionResults


class FirstTest002(LoginPage):
    """
    FirstTest002 - class with test case methods
    """
    def __init__(self, log_path: str):
        """
        init function to get all the JSON variables
        :param log_path: path of the log file
        """
        super().__init__(log_path)
        self.url = json_load.get('url_main')
        self.url_login = json_load.get('url_login')
        self.url_logged = json_load.get('url_logged')
        self.username = json_load.get('username')
        self.password = json_load.get('password')
        self.username_field = tuple(json_load.get('username_field'))
        self.password_field = tuple(json_load.get('password_field'))
        self.submit_field = tuple(json_load.get('submit_button'))
        self.cache_remove_button = tuple(json_load.get('cache').get('remove_button'))
        self.cache_existing = tuple(json_load.get('cache').get('existing'))
        self.cache_my_account = tuple(json_load.get('cache').get('my_account_button'))

    def __enter__(self):
        """
        enter function to set start time and print log file header
        """
        self.start_time = time.time()
        self.log(log_header)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exit function to measure and print test case time
        """
        self.end_time = time.time()
        self.log(("Test Case {} elapsed time: {:.3f} s\n".format(test_name[0], self.end_time-self.start_time)))

    def pre_condition_01(self):
        """
        pre_condition_01 function to open, accept cache and check that cache is not displayed on the page

        :return: returns PASS if cache is not displayed, otherwise returns ERROR
        """
        self.open_page(self.url)
        self.accept_cache(self.cache_remove_button)
        if self.check_cache_disabled(self.cache_existing):
            self.log("{}: PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("{}: ERROR\n".format(currentframe().f_code.co_name))
            return ExecutionResults.ERROR

    def test_case_01(self):
        """
        test_case_01 function to open my account and verify that expected page is opened

        :return: returns PASS if expected page matches current page, otherwise returns FAIL
        """
        self.go_to_my_account(self.cache_my_account)
        if self.verify_part_url(self.url_login):
            self.log("{}: PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("{}: FAIL\n".format(currentframe().f_code.co_name))
            return ExecutionResults.FAIL

    def test_case_02(self):
        """
        test_case_02 function to log in and verify that expected page is opened

        :return: returns PASS if expected page matches current page, otherwise returns FAIL
        """
        self.type_username(self.username_field, self.username)
        self.type_password(self.password_field, self.password)
        self.click_submit(self.submit_field)
        if self.verify_full_url(self.url_logged):
            self.log("{}: PASS\n".format(currentframe().f_code.co_name))
            return ExecutionResults.PASS
        else:
            self.log("{}: FAIL\n".format(currentframe().f_code.co_name))
            return ExecutionResults.FAIL

    def post_condition_01(self):
        """
        post_condition_01 function to pass

        :return: returns PASS
        """
        self.log("{}: PASS\n".format(currentframe().f_code.co_name))
        return ExecutionResults.PASS


# if executed is test_run.py file then test name is assigned from 'actual_opened_filename' variable from test_run.py
# file, otherwise test name is name of the executed file in configuration
if (basename(__file__)) == 'test_run.py':
    test_name = currentframe().f_globals.get('actual_opened_filename').split(".")
else:
    test_name = basename(__file__).split(".")

init = InitClass()

# name of imported JSON
json_name = argv[1]
# get log path, header and json file from initialization function of InitClass
path_log, log_header, json_load = init.test_initialization(test_name, json_name)

# open test case as context manager
with FirstTest002(path_log) as first_test_002:
    init.execute_all_methods(first_test_002)