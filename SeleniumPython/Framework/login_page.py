from SeleniumPython.Framework.base_page import BasePage

class LoginPage(BasePage):
    """
    LoginPage - class with all the methods around login into page
    """

    def __init__(self, log_path: str):
        """

        :param log_path: path of the log file
        """
        super().__init__(log_path)

    def open_page(self, url: str):
        """
        function to open url

        :param url: string url page

        """
        self.log("Opening url: {}".format(url))
        self._open_url(url)

    def verify_full_url(self, expected_page: str) -> bool:
        """
        function to verify that expected url is matching current url

        :param expected_page: string url expected page

        :return: returns true value if expected page url match current page url, otherwise returns false
        """
        current_url = self.current_url
        self.log("Expected url: {}, current URL: {}".format(expected_page, current_url))
        return current_url == expected_page

    def verify_part_url(self, expected_page: str) -> bool:
        """
        function to verify that expected url is in current url

        :param expected_page: string url expected page

        :return: returns true value if expected page url match current page url, otherwise returns false
        """
        current_url = self.current_url
        self.log("Current URL: {}".format(current_url))
        return expected_page in current_url

    def accept_cache(self, locator: tuple):
        """
        function to accept all cache by clicking locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        """
        self.log("Accepting cache")
        self._click(locator)

    def check_cache_disabled(self, locator: tuple) -> bool:
        """
        function to check that cache locator is disabled (not displayed)

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))

        :return: returns true value if locator is not displayed, otherwise returns false
        """
        self.log("Checking that Cache was disabled")
        return not self._is_displayed(locator)

    def go_to_my_account(self, locator: tuple):
        """
        function to go to my account by clicking locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        """
        self.log("Going into my account")
        self._click(locator)

    def type_username(self, username_field: tuple, username: str):
        """
        function to type username into username locator

        :param username_field: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param username: username declared to type
        """
        self.log("Typing username: {} into username field".format(username))
        self._type(username_field, username)

    def type_password(self, password_field: tuple, password: str):
        """
        function to type password into password locator

        :param password_field: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param password: password declared to type
        """
        self.log("Typing password into password field")
        self._type(password_field, password)

    def click_submit(self, submit_button: tuple):
        """
        function to submit button by clicking locator

        :param submit_button: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        """
        self.log("Clicking submit button")
        self._click(submit_button)

    def log(self, x: str):
        """
        function to print text on the terminal and in opened log file

        :param x: string of the text to print

        """
        self._log(x)


