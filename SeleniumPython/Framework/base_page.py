from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.chrome.options import Options

class BasePage:
    """
    BasePage - class with all the selenium methods
    """
    def __init__(self, log_path):
        """

        :param log_path: path of the log file
        """
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self._driver = webdriver.Chrome(options=chrome_options)
        self.log_path = log_path

    def _find(self, locator: tuple) -> WebElement:
        """
        function to find the locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))

        :return: returns locator element
        """
        return self._driver.find_element(*locator)

    def _type(self, locator: tuple, text: str, time: int = 10):
        """
        function to type string into locator element

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        self._wait_until_element_is_visible(locator, time)
        self._find(locator).send_keys(text)

    def _wait_until_element_is_visible(self, locator: tuple|tuple[str, str], time: int = 10):
        """
        function to check if element locator is visible until time expire

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        wait = WebDriverWait(self._driver, time)
        wait.until(ec.visibility_of_element_located(locator))

    def _wait_until_element_is_presented(self, locator: tuple|tuple[str, str], time: int = 10):
        """
        function to check if element locator is presented until time expire

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        wait = WebDriverWait(self._driver, time)
        wait.until(ec.presence_of_element_located(locator))

    def _wait_until_element_is_clickable(self, locator: tuple|tuple[str, str], time: int = 10):
        """
        function to check if element locator is clickable until time expire

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        wait = WebDriverWait(self._driver, time)
        wait.until(ec.element_to_be_clickable(locator))

    def _click(self, locator: tuple, time: int = 10):
        """
        function to click element locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        self._wait_until_element_is_visible(locator, time)
        self._find(locator).click()

    def _get_text(self, locator: tuple, time: int = 10) -> str:
        """
        function to get text of the element locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        :return: returns string of the element locator text
        """
        self._wait_until_element_is_visible(locator, time)
        return self._find(locator).text

    def _clear_text(self, locator: tuple, time: int = 10):
        """
        function to clear text on the element locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param time: time in seconds until Exception TimeoutError appears

        """
        self._wait_until_element_is_visible(locator, time)
        element = self._find(locator)
        self._wait_until_element_is_clickable(locator, time)
        element.clear()

    def _get_attribute(self, locator: tuple, attribute: str) -> str:
        """
        function to get attribute of the element locator

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))
        :param attribute: get attribute of the locator element

        :return: returns string of the element locator attribute
        """
        return self._find(locator).get_attribute(attribute)

    def _is_displayed(self, locator: tuple) -> bool:
        """
        function to check if element locator is displayed

        :param locator: tuple with locator strategy amd locator itself (e.g., ("id", "id_name"))

        :return: returns true if element locator is displayed, otherwise returns false
        """
        try:
            return self._find(locator).is_displayed()
        except NoSuchElementException:
            return False

    def _open_url(self, url: str):
        """
        function to open url

        :param url: string url page

        """
        self._driver.get(url)
        self._driver.maximize_window()

    @property
    def current_url(self) -> str:
        """
        function to get current url

        :return: returns string of the current url
        """
        return self._driver.current_url

    def _log(self, x: str):
        """
        function to print text on the terminal and in opened log file

        :param x: string of the text to print

        """
        with open(self.log_path, "a") as file:
            file.write(x)
            print(x)
            file.write("\n")
