import enum

class ExecutionResults(enum.Enum):
    """
    ExecutionResults - class with execution results for Test Cases
    """
    PASS = 1
    FAIL = 2
    ERROR = 3
    UNKNOWN = 99