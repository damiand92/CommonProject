from datetime import datetime
from inspect import ismethod
from json import load
from os.path import basename, join
from pathlib import Path
from typing import Any
from SeleniumPython.Framework.enum import ExecutionResults


class InitClass:
    """
    InitClass - class with all the init methods
    """

    @staticmethod
    def test_initialization(test_name: str, json_name: str) -> tuple[str, str, Any]:
        """
        function to set all the initialization for test case

        :param test_name: string of test case name
        :param json_name: string of json name

        :return: returns tuple of two strings for log file path, log header and returns json file loaded data
        """
        # if executed is test_run.py file then main path for Log directory is only one level up, otherwise if executed
        # is directly test case, then main path should be two level up
        if (basename(__file__)) == 'test_run.py':
            main_path = Path(__file__).parents[0]
        else:
            main_path = Path(__file__).parents[1]

        logs_dir = 'Logs'
        log_dir = join(main_path, logs_dir)
        # date and time was taken for the name of the log file
        date_and_time = str(datetime.now()).replace(' ', '_').replace(':', '-')

        log_file_path = r"{}_{}.txt".format(test_name[0], date_and_time[:-7])
        log_path = join(log_dir, log_file_path)
        log_header = (
            "Test name:\t\t\t\t{}\nStart time:\t\t\t\t{}\nDirectory log:\t\t\t\t{}\n\n\n".format(test_name[0],
                                                                                               date_and_time,
                                                                                               log_dir))

        json_directory = r'JSON'
        json_name = join(json_directory, json_name)
        path_json = join(main_path, json_name)
        # importing all the JSON data from declared file
        with open(path_json, mode="r", encoding="utf-8") as read_file:
            json_load = load(read_file)

        return log_path, log_header, json_load

    @staticmethod
    def execute_all_methods(instance: object):
        """
        function to execute all the methods from the instance of test case class

        :param instance: instance of test case class
        """
        # taking all the attributes of the instance function
        x = (getattr(instance, name) for name in dir(instance))
        # verifying if attribute is a method
        methods = filter(ismethod, x)

        # listing all the methods which names are as expected
        pre_methods = list()
        post_methods = list()
        test_methods = list()
        for method in methods:
            if 'pre_condition' in method.__name__:
                pre_methods.append(method)
            elif 'test_case' in method.__name__:
                test_methods.append(method)
            elif 'post_condition' in method.__name__:
                post_methods.append(method)

        try:
            for pre in pre_methods:
                if pre() == ExecutionResults.ERROR:
                    raise Exception("Error occurred during {} method. All the methods will be omitted except "
                                    "for post conditions. ".format(pre.__name__))
        except Exception as e:
            print(e)
        else:
            for test in test_methods:
                test()
        finally:
            for post in post_methods:
                post()


