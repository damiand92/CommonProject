# Use the Ubuntu 24.04 base image with Python 3.12 and update package lists for the Ubuntu system
FROM ubuntu:24.04
FROM python:3.12
RUN apt-get update

# Copy and install the Chrome Debian package
COPY chrome_114_amd64.deb ./
RUN apt install ./chrome_114_amd64.deb -y

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# set display port to avoid crash
ENV DISPLAY=:99

# Set the working directory inside the image to /app and copy requirements.txt
WORKDIR /app
COPY requirements.txt /app/requirements.txt

# Upgrade pip and install python dependencies listed in requirements.txt
RUN pip install --upgrade pip -q
RUN pip install -r /app/requirements.txt -q